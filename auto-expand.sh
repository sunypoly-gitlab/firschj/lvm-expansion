#!/usr/bin/env bash
EXIT_GENERIC_FAIL=1
EXIT_NOT_ROOT=2
EXIT_DRIVE_DNE=4
EXIT_NO_SPACE=8

DRIVE=${1:-/dev/sda}
VG=${2:-ubuntu-vg}
LV_PATH=${3:-/dev/ubuntu-vg/ubuntu-lv}

if [ "$(whoami)" != "root" ]; then
    >&2 echo "You must run this script as root. Prefix your previous command with sudo."
    exit $EXIT_NOT_ROOT
fi

if ! ls ${DRIVE} >/dev/null 2>&1; then
    >&2 echo "Unable to find drive $DRIVE"
    exit $EXIT_DRIVE_DNE
fi

end_of_disk=$(parted -s $DRIVE print free | sed -e 's/\s\+/\t/g' -e '/^$/d' | tail -n 1)

echo "$end_of_disk" | grep -E "Free\s+Space"
if [ $? -ne 0 ]; then
    echo "No free space found. Add space to the $DRIVE drive first."
    exit $EXIT_NO_SPACE
fi

FDISK_CMDS="
n



t

31
w
"
fdisk $DRIVE <<< "$FDISK_CMDS"

last_part=$(parted -s $DRIVE print free | sed -e 's/\s\+/\t/g' -e '/^$/d' -e 's/^\s*//' | tail -n 1 | cut -f1)
echo $DRIVE$last_part
pvcreate $DRIVE$last_part
vgextend $VG $DRIVE$last_part
lvextend -r -l +100%FREE $LV_PATH
