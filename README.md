# LVM Expansion Script

This script uses `fdisk` and `parted` to expand a logical volume using unused
space on a disk. This script must be run as the root user.

By default:

* The disk sourced for unused space is `/dev/sda`
* The volume group is named `ubuntu-vg`
* The logical volume path expanded is `/dev/ubuntu-vg/ubuntu-lv`

## Usage

To use this script with the defaults, run the below command.

```sh
sudo ./auto-expand.sh
```

This script does accept 3 positional arguments in the form described below for
any overrides required on your system.

```sh
sudo ./auto-expand.sh [DISK] [VOLUME GROUP] [LOGICAL VOLUME PATH]
```
